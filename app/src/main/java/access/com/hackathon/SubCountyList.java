package access.com.hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class SubCountyList extends AppCompatActivity {

    public String county_name;

    private ProgressDialog pDialog;

    ArrayList<HashMap<String, String>> subCountiesList;

    JSONArray subCounties = null;
    ListView subCountyList;

    public String API_URL = "https://protected-falls-20828.herokuapp.com/locations/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_county_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent getCounty = getIntent();

        county_name = getCounty.getStringExtra("County");


        subCountyList = (ListView)findViewById(R.id.subCountyList);

        subCountyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String sub_county_name = ((TextView)view.findViewById(R.id.sub_county_name)).getText().toString();

                //Starting new intent
                Intent startFacility = new Intent(getApplicationContext(),FacilityList.class);
                startFacility.putExtra("subCounty",sub_county_name);
                startActivity(startFacility);
            }
        });

        subCountiesList = new ArrayList<HashMap<String, String>>();

        //progressBar = (ProgressBar)findViewById(R.id.progressBar);

        new RetrieveSubCounties().execute();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    class RetrieveSubCounties extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(SubCountyList.this);
            pDialog.setMessage("Loading sub counties. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //String noEncoding = API_URL+county_name+"/subcounty";
                //String encodedURL = URLEncoder.encode(","UTF-8").;

                URL url = new URL(API_URL+county_name+"/subcounty/");
                Log.d("encX",url.toString());

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //urlConnection.setRequestProperty("connection", "close");
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }


                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("Error:Reading Back", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }

            try {

                JSONArray subCounties = new JSONArray(response);

                for (int i = 0; i < subCounties.length(); i++) {
                    JSONObject c = subCounties.getJSONObject(i);

                    // Storing each json item in variable
                    String sub_county_name = c.getString("name");

                    // creating new HashMap
                    HashMap<String, String> countyMap = new HashMap<String, String>();

                    // adding each child node to HashMap key => value

                    countyMap.put("sub_county_name", sub_county_name);

                    // adding HashList to ArrayList
                    subCountiesList.add(countyMap);
                }

            } catch (JSONException e) {
                // Appropriate error handling code
            }

            pDialog.dismiss();

            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    ListAdapter adapter = new SimpleAdapter(
                            SubCountyList.this, subCountiesList,
                            R.layout.sub_county_list_item, new String[] { "sub_county_name"},
                            new int[] {R.id.sub_county_name });
                    // updating listview
                    subCountyList.setAdapter(adapter);
                }
            });


            Log.d("rext", response);
        }
    }

}
