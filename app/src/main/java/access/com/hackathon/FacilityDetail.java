package access.com.hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class FacilityDetail extends AppCompatActivity {

    Button viewMap,rateFacility;

    TextView facilityCounty,facilitySubCounty,facilityName,facilityCode,facilityType,facilityBeds,facilityCots,facilityRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewMap = (Button)findViewById(R.id.showMap);

        viewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapper = new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(mapper);
            }
        });
        rateFacility = (Button)findViewById(R.id.rateButton);

        facilityCounty = (TextView)findViewById(R.id.facilityCounty);
        facilitySubCounty = (TextView)findViewById(R.id.facilitySubCounty);
        facilityName = (TextView)findViewById(R.id.facilityName);
        facilityCode = (TextView)findViewById(R.id.facilityCode);
        facilityType = (TextView)findViewById(R.id.facilityType);
        facilityBeds = (TextView)findViewById(R.id.facilityBeds);
        facilityCots = (TextView)findViewById(R.id.facilityCots);
        facilityRating = (TextView)findViewById(R.id.facilityRating);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

//    class RetrieveFacilities extends AsyncTask<Void,Void,String> {
//
//        @Override
//        protected void onPreExecute() {
//            pDialog = new ProgressDialog(FacilityList.this);
//            pDialog.setMessage("Loading facilities. Please wait...");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(Void... params) {
//            try {
//                URL url = new URL(API_URL+sub_county_name+"/");
//
//                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                //urlConnection.setRequestProperty("connection", "close");
//                try {
//                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String line;
//                    while ((line = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(line).append("\n");
//                    }
//
//
//                    bufferedReader.close();
//                    return stringBuilder.toString();
//                } finally {
//                    urlConnection.disconnect();
//                }
//            } catch (Exception e) {
//                Log.e("Error:Reading Back", e.getMessage(), e);
//                return null;
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String response) {
//            if (response == null) {
//                response = "THERE WAS AN ERROR";
//            }
//
//            try {
//
//                JSONArray facilities = new JSONArray(response);
//
//                for (int i = 0; i < facilities.length(); i++) {
//                    JSONObject c = facilities.getJSONObject(i);
//
//                    // Storing each json item in variable
//                    String facility_name = c.getString("name");
//
//                    // creating new HashMap
//                    HashMap<String, String> countyMap = new HashMap<String, String>();
//
//                    // adding each child node to HashMap key => value
//
//                    countyMap.put("facility_name", facility_name);
//
//                    // adding HashList to ArrayList
//                    facilitiesList.add(countyMap);
//                }
//
//            } catch (JSONException e) {
//                // Appropriate error handling code
//            }
//
//            pDialog.dismiss();
//
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    /**
//                     * Updating parsed JSON data into ListView
//                     * */
//
//                    ListAdapter adapter = new SimpleAdapter(
//                            FacilityList.this, facilitiesList,
//                            R.layout.facility_list_item, new String[] { "facility_name"},
//                            new int[] {R.id.facility_name });
//                    // updating listview
//                    facilityList.setAdapter(adapter);
//                }
//            });
//
//
//            Log.d("rext", response);
//        }
//    }

}
