package access.com.hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.*;

public class CountyList extends AppCompatActivity {

    public ProgressBar progressBar;
    private ProgressDialog pDialog;

    public static String API_URL  = "https://protected-falls-20828.herokuapp.com/locations/county";

    ArrayList<HashMap<String, String>> countiesList;

    JSONArray counties = null;
    ListView countyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_county_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        PrimaryDrawerItem User  = new PrimaryDrawerItem().withName("Orders");
//        PrimaryDrawerItem customersItem = new PrimaryDrawerItem().withName(R.string.drawer_item_customers);
//        SecondaryDrawerItem orderRecipientsItem = new SecondaryDrawerItem().withName(R.string.drawer_item_recipients);
//        SecondaryDrawerItem productsItem = new SecondaryDrawerItem().withName(R.string.drawer_item_products);
//        PrimaryDrawerItem settingsItem = new PrimaryDrawerItem().withIcon(R.drawable.ic_settings_black_48dp).withName(R.string.drawer_item_settings);

//        Drawer result = new DrawerBuilder()
//                .withActivity(this)
//                .withToolbar(toolbar)
//                .withAccountHeader(headerResult)
//                .withTranslucentStatusBar(false)
//                .withActionBarDrawerToggle(true)
//                .addDrawerItems(
//                        orderListItem,
//                        customersItem,
//                        new DividerDrawerItem(),
//                        orderRecipientsItem,
//                        //productsItem,
//                        new DividerDrawerItem(),
//                        settingsItem
//                )
//                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
//                    @Override
//                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        // do something with the clicked item :D
//                        if (position == 1) {
//                            Intent orderListIntent = new Intent(CountyList.this, OrderList.class);
//                            startActivity(orderListIntent);
//                        } else if (position == 2) {
//                            Intent customerIntent = new Intent(CountyList.this, CustomerList.class);
//                            startActivity(customerIntent);
//                        } else if (position == 4) {
//                            Intent recipientIntent = new Intent(CountyList.this, RecipientList.class);
//                            startActivity(recipientIntent);
//                        }
//
//                        return true;
//                    }
//                })
//                .build();

        countyList = (ListView)findViewById(R.id.countyList);

        countyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String county_name = ((TextView)view.findViewById(R.id.county_name)).getText().toString();

                //Starting new intent
                Intent startSubCounty = new Intent(getApplicationContext(),SubCountyList.class);
                startSubCounty.putExtra("County",county_name);
                startActivity(startSubCounty);
            }
        });

        countiesList = new ArrayList<HashMap<String, String>>();

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        new RetrieveCounties().execute();

       //countyList.setAdapter(adapter);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    class RetrieveCounties extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            pDialog = new ProgressDialog(CountyList.this);
            pDialog.setMessage("Loading counties. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(API_URL);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //urlConnection.setRequestProperty("connection", "close");
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("Error:Reading Back", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }

            try {

                JSONArray counties = new JSONArray(response);

                for (int i = 0; i < counties.length(); i++) {
                    JSONObject c = counties.getJSONObject(i);

                    // Storing each json item in variable

                    String county_name = c.getString("name");

                    // creating new HashMap
                    HashMap<String, String> countyMap = new HashMap<String, String>();

                    // adding each child node to HashMap key => value

                    countyMap.put("county_name", county_name);

                    // adding HashList to ArrayList
                    countiesList.add(countyMap);
                }

            } catch (JSONException e) {
                // Appropriate error handling code
            }

            pDialog.dismiss();

            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    ListAdapter adapter = new SimpleAdapter(
                            CountyList.this, countiesList,
                            R.layout.county_list_item, new String[] { "county_name"},
                            new int[] { R.id.county_name });
                    // updating listview
                    countyList.setAdapter(adapter);
                }
            });



            Log.d("rext", response);
            //progressBar.setVisibility(View.GONE);
            //Log.i("INFO", response);

        }
    }

}
