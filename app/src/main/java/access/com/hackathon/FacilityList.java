package access.com.hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FacilityList extends AppCompatActivity {

    public String sub_county_name;

    public ProgressDialog pDialog;

    ArrayList<HashMap<String, String>> facilitiesList;

    JSONArray facilities = null;
    ListView facilityList;

    public String API_URL = "https://protected-falls-20828.herokuapp.com/facilities/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Intent getCounty = getIntent();

        sub_county_name = getCounty.getStringExtra("subCounty");


        facilityList = (ListView)findViewById(R.id.facilityList);

        facilityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String county_name = ((TextView)view.findViewById(R.id.facility_name)).getText().toString();

                //Starting new intent
                Intent startFacility = new Intent(getApplicationContext(),FacilityDetail.class);
                //startFacility.putExtra("County",sub_county_name);
                startActivity(startFacility);
            }
        });

        facilitiesList = new ArrayList<HashMap<String, String>>();

        //progressBar = (ProgressBar)findViewById(R.id.progressBar);

        new RetrieveFacilities().execute();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    class RetrieveFacilities extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(FacilityList.this);
            pDialog.setMessage("Loading facilities. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(API_URL+sub_county_name+"/");

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //urlConnection.setRequestProperty("connection", "close");
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }


                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("Error:Reading Back", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }

            try {

                JSONArray subCounties = new JSONArray(response);

                for (int i = 0; i < subCounties.length(); i++) {
                    JSONObject c = subCounties.getJSONObject(i);

                    // Storing each json item in variable
                    String sub_county_name = c.getString("name");

                    // creating new HashMap
                    HashMap<String, String> countyMap = new HashMap<String, String>();

                    // adding each child node to HashMap key => value

                    countyMap.put("sub_county_name", sub_county_name);

                    // adding HashList to ArrayList
                    facilitiesList.add(countyMap);
                }

            } catch (JSONException e) {
                // Appropriate error handling code
            }

            pDialog.dismiss();

            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */

                    ListAdapter adapter = new SimpleAdapter(
                            FacilityList.this, facilitiesList,
                            R.layout.facility_list_item, new String[] { "sub_county_name"},
                            new int[] {R.id.facility_name });
                    // updating listview
                    facilityList.setAdapter(adapter);
                }
            });


            Log.d("rext", response);
        }
    }

}
